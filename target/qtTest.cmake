include(${CMAKE_CURRENT_LIST_DIR}/executable.cmake)

opensdr_check_var(OPENSDR_QT_ADDONS_ENABLED IS_TRUE)
opensdr_check_var(OPENSDR_QT_TEST_APP_ENABLED IS_TRUE)

macro (add_opensdr_test_qt TargetName)
  add_opensdr_executable(${TargetName}
    INC_DIRS
      "${OPENSDR_DIR}/Qt_Addons/internal"
    RESET_MODE
    ${ARGN})
endmacro()
 
