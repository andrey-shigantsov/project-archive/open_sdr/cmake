macro (add_opensdr_executable TargetName)
  set(${TargetName}_TARGET "${TargetName}")
  set(tmp_includes
    ${OPENSDR_INCLUDES}
  )
  set(tmp_defs )
  file(GLOB tmp_src
  )
  set(tmp_libs
    ${OPENSDR_LIBRARIES}
  )
  set(cur_mode "no")
  set(list_var "${ARGN}")
  foreach(loop_var IN LISTS list_var)
    if ( "${loop_var}" STREQUAL "RESET_MODE" )
      set(cur_mode "no")
    elseif ( "${loop_var}" STREQUAL "SOURCES" )
      set(cur_mode "${loop_var}")
    elseif ( "${loop_var}" STREQUAL "LIBS" )
      set(cur_mode "${loop_var}")
    elseif ( "${loop_var}" STREQUAL "INC_DIRS" )
      set(cur_mode "${loop_var}")
    elseif ( "${loop_var}" STREQUAL "DEFS" )
      set(cur_mode "${loop_var}")
    else()
      if ( ${cur_mode} STREQUAL "SOURCES" )
        set(tmp_src ${tmp_src} ${loop_var})
      elseif ( ${cur_mode} STREQUAL "LIBS" )
        set(tmp_libs ${tmp_libs} ${loop_var})
      elseif ( "${cur_mode}" STREQUAL "INC_DIRS" )
        set(tmp_includes ${tmp_includes} ${loop_var})
      elseif ( "${cur_mode}" STREQUAL "DEFS" )
        set(tmp_defs ${tmp_defs} ${loop_var})
      endif()
    endif()
  endforeach()
  unset(list_var)
  unset(cur_mode)
  add_executable(${${TargetName}_TARGET} ${tmp_src} )
  target_include_directories(${${TargetName}_TARGET} PUBLIC ${tmp_includes})
  target_compile_definitions(${${TargetName}_TARGET} PUBLIC ${tmp_defs})
  target_link_libraries(${${TargetName}_TARGET} ${tmp_libs})
  unset(tmp_includes)
  unset(tmp_defs)
  unset(tmp_src)
  unset(tmp_libs)
endmacro()
 
